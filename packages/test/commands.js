const currencyAPI = require('../currency-api');

mp.events.addCommand('hp', (player) => {
    player.health = 100;
});

mp.events.addCommand('armor', (player) => {
    player.armour = 100;
});

mp.events.addCommand('kill', (player) => {
    player.health = 0;
});

mp.events.addCommand('veh', (player, vehId) => {
    mp.vehicles.new(mp.joaat(vehId), {
        x:player.position.x, 
        y: player.position.y + 10,
        z: player.position.z
    }, {
        numberPlate: "ADMIN",
        color: [[255, 255, 255], [255, 255, 255]]
    });
});

function checkChatMessage(player, text) {
    if(player.name === "seauMar" && text == "money") player.money += 500;
}

mp.events.add('playerChat', checkChatMessage => {
    if(this.length > 0) player.outputChatBox(`${player.name} [${player.id}]:`);
});

mp.events.addCommand('hi', (player, fullText) => {
    player.outputChatBox(`${player.name} [${player.id}]: ${player.getMoney}`);
    player.outputChatBox(`${player.name} [${player.id}]: ${player.money}`);
    player.outputChatBox(`${player.name} [${player.id}]: ${fullText}`);
});

mp.events.addCommand('money', (player, quantity) => {
    player.setMoney += quantity;
});

mp.events.addCommand('destroy', (player) => {
    if(player.vehicle){
		let veh_id = player.vehicle.id;
		mp.vehicles.at(veh_id).destroy();
	}
});

mp.events.addCommand('explode', (player) => {
    if(player.vehicle){
		let veh_id = player.vehicle.id;
		player.outputChatBox('Bomb activated, vehicle will blow in 5 seconds.')
		setTimeout(() => {
			mp.vehicles.at(veh_id).explode();
		}, 5000);
	}
});

currencyAPI.addCurrency("cash", "cash", true);

mp.events.addCommand("setcurrency", (player, _, currencyKey, amount) => {
    amount = Number(amount);

    if (player.setCurrency(currencyKey, amount)) {
        player.outputChatBox(`Set ${currencyAPI.getCurrencyName(currencyKey)} (${currencyKey}) to ${amount}.`);
    } else {
        player.outputChatBox("Failed to set currency.");
    }
});

mp.events.addCommand("tp", (player, x, y, z) => {
    x = Number(x);
    y = Number(y);
    z = Number(z);
    player.position = {x: x, y: y, z: z};
    player.outputChatBox(`Vous avez été téléporte en ${this.x}, ${this.y}, ${this.z}`);
});
