let speedometerWindow = null;
let player = mp.players.local;

mp.events.add('playerEnterVehicle', () => {
   
    if(speedometerWindow != null)
	{
		speedometerWindow.destroy();
		speedometerWindow = null;
	}
	speedometerWindow = mp.browsers.new("package://speedometer/index.html");
});


mp.events.add('playerLeaveVehicle', () =>
{
	if(speedometerWindow != null)
	{
		speedometerWindow.destroy();
		speedometerWindow = null;
	}
});

mp.events.add('render', () =>
{
	if(player.vehicle != null && speedometerWindow != null)
	{
		let speed = player.vehicle.getSpeed() * 3.6;
		speedometerWindow.execute(`setSpeed(${speed});`);
	}
});