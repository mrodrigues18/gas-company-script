require('./gamemode/discord.js');

mp.events.call('setDiscordStatus', 'Developing biz script', 'Local server');

mp.events.addDataHandler("currency_cash", (entity, value) => {
    if (entity.handle === mp.players.local.handle) {
        mp.game.stats.statSetInt(mp.game.joaat("SP0_TOTAL_CASH"), value, false);
        mp.gui.chat.push(`(clientside) currency_cash updated, new value: ${value}`);
    }
});